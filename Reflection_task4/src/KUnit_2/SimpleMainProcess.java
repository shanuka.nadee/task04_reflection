package KUnit_2;

public class SimpleMainProcess {
	  public double X = 20;
	  private double Y = 30;
	  
	  public String firstName = "Sanuka";
	  private String lastName = "Nadeeshan";
	  
	  
	  public SimpleMainProcess(){}
	  
	  public SimpleMainProcess(double c, double d) {
		  this.X = c;
		  this.Y = d;
	  }
	  
	  public void squareC() {
		  this.X *= this.Y;
	  }
	  

	  public double get_val1() {
		  return X;
	  }
	  
	  public double get_val2() {
		  return Y;
	  }
	   
	  public String toString() {
		  return String.format("(c:%f, d:%f)", X, Y);
	  }
	  
	  public SimpleMainProcess(String str1, String str2) {
		  this.firstName = str1;
		  this.lastName = str2;
	  }
	  
	  public void StringClassA() {
		  this.firstName += this.firstName;
	  }
	  
	  public String getClassA() {
		  return firstName;
	  }
	  
	  public void StringClassB() {
		  this.lastName += this.lastName;
	  }
	  
	  public String getClassB() {
		  return lastName;
	  }
	  
	  public double getC() {
		  return X;
	  }
	  
	  public void setC(double c) {
		  this.X = c;
	  }
	  
	  public double getD() {
		  return Y;
	  }
	  
	  public void setD(double d) {
		  this.Y = d;
	  }
}
