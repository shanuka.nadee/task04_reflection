package KUnit_2;

import static KUnit_1.KUnit01.*;
import java.io.IOException;
import java.sql.SQLException;


public class SimpleMainTest {
	
	public static void main(String args[]) throws IOException, SQLException{
		SimpleMainProcess s = new SimpleMainProcess(3.5, 2.5);
		
		checkEquals(s.getC(), 3.5);
		checkNotEquals(s.getD(), 2.5);
		checkEquals(s.getD(), 2.5);
		
		SimpleMainProcess str = new SimpleMainProcess("Shanuka", "Nadeeshan");
		checkEquals(str.getClassA(), "Nadeeshan");
	    
		report();	
	}
}

