package KUnit_2;

import java.lang.reflect.Method;

public class Reflection10 {
	public static void main(String args[]) throws Exception{
		SimpleMainProcess simpMain = new SimpleMainProcess();
		//Get methods data
		Method methodData = simpMain.getClass().getDeclaredMethod("setC",double.class);
		methodData.setAccessible(true);
		methodData.invoke(simpMain, 50);
		System.out.println(simpMain);
	}
}
