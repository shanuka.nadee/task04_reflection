package JUnitTest;

import static org.junit.jupiter.api.Assertions.*;

import java.lang.reflect.Field;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import KUnit_2.SimpleMainProcess;

class Ref4 {


	@Test
	void test() {
		public static void main(String args[]) throws Exception{
			SimpleMainProcess simpMain = new SimpleMainProcess();
			//Get Field Declaration into array
			Field[] fieldArray = simpMain.getClass().getDeclaredFields();
			//Show Field Details through the Loop
			System.out.printf("This class have %d Fields\n", fieldArray.length);
			for(Field f : fieldArray) {
				f.setAccessible(true);
				System.out.printf("This Field Name is =%s, Type is =%s, Value is =%f\n",f.getName(), f.getType(), f.getDouble(simpMain));
			}
		}
		
	}
